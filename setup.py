# Automatically created by: shub deploy

from setuptools import setup, find_packages

setup(
    name         = 'itprice',
    version      = '1.0',
    packages     = find_packages(),
    entry_points = {'scrapy': ['settings = itprice.settings']},
    package_data={
        'itprice': ['*.txt', 'itprice/*.txt', '*.zip', 'itprice/*.zip']
    },
    zip_safe=False,
)
