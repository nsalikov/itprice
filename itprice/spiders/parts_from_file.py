# -*- coding: utf-8 -*-
import re
import scrapy
from zipfile import ZipFile
from itertools import islice
from scrapy.exceptions import CloseSpider


class PartsFromFileSpider(scrapy.Spider):
    """
    Usage:
        scrapy crawl parts_from_file -a f=0 -a t=432000
        scrapy crawl parts_from_file -a f=432000 -a t=864000
        scrapy crawl parts_from_file -a f=432000 -a t=864000
        scrapy crawl parts_from_file -a f=864000 -a t=1296000
        scrapy crawl parts_from_file -a f=1296000 -a t=2160000
        scrapy crawl parts_from_file -a f=2592000 -a t=2593266

        or just:
        scrapy crawl parts_from_file -a f=2592000

        300-500 estimated items per minute * 60 * 24 = 432000-720000 items per day (scrapinghub's limit)
    """

    name = 'parts_from_file'
    allowed_domains = ['itprice.com']

    f = None # from line in URLS_FILE_ARCHIVE/URLS_FILE_NAME
    t = None # to line in URLS_FILE_ARCHIVE/URLS_FILE_NAME


    def start_requests(self):
        if self.f:
            self.f = int(self.f)
        if self.t:
            self.t = int(self.t)

        if 'URLS_FILE_ARCHIVE' not in self.settings or 'URLS_FILE_NAME' not in self.settings:
            raise CloseSpider("You must specificy URLS_FILE_ARCHIVE and URLS_FILE_NAME file in settings.py")

        archive = self.settings['URLS_FILE_ARCHIVE']
        filename = self.settings['URLS_FILE_NAME']

        with ZipFile(archive) as myzip:
            with myzip.open(filename) as myfile:
                lines = islice(myfile.readlines(), self.f, self.t)
                for line in lines:
                    url = line.decode("utf-8").strip()
                    yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        d = {}

        d['url'] = response.url
        d['breadcrumbs'] = response.css('ol.breadcrumb li a ::text').extract()
        d['name'] = response.css('.main h1 ::text').extract_first()
        d['specs'] = dict(parse_specs(response))

        if d['name']:
            d['name'] = d['name'].strip()

        yield d


def parse_specs(response):
    for tr in response.css('table.table-striped-product tr'):
        key = ' '.join(filter(None, [t.replace('\xa0', ' ').strip() for t in tr.css('td:nth-child(1) ::text').extract()]))
        value = ' '.join(filter(None, [t.replace('\xa0', ' ').strip() for t in tr.css('td:nth-child(2) ::text').extract()]))

        if key == "Choose End User's Country":
            continue

        if 'price alert' in value.lower():
            value = re.sub('\s*Price Alert\s*', '', value, flags=re.IGNORECASE).strip()

        if key:
            yield (key, value)
