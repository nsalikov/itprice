# -*- coding: utf-8 -*-
import re
import scrapy
from scrapy.spiders import SitemapSpider


class PartsSitemapSpider(SitemapSpider):
    name = 'parts_sitemap'
    allowed_domains = ['itprice.com']

    sitemap_urls = ['https://itprice.com/sitemap.xml']
    sitemap_rules = [('itprice.com/[^/]+/[^/]+\.htm', 'parse')]


    def parse(self, response):
        d = {}

        d['url'] = response.url
        d['breadcrumbs'] = response.css('ol.breadcrumb li a ::text').extract()
        d['name'] = response.css('.main h1 ::text').extract_first()
        d['specs'] = dict(parse_specs(response))

        if d['name']:
            d['name'] = d['name'].strip()

        yield d


def parse_specs(response):
    for tr in response.css('table.table-striped-product tr'):
        key = ' '.join(filter(None, [t.replace('\xa0', ' ').strip() for t in tr.css('td:nth-child(1) ::text').extract()]))
        value = ' '.join(filter(None, [t.replace('\xa0', ' ').strip() for t in tr.css('td:nth-child(2) ::text').extract()]))

        if key == "Choose End User's Country":
            continue

        if 'price alert' in value.lower():
            value = re.sub('\s*Price Alert\s*', '', value, flags=re.IGNORECASE).strip()

        if key:
            yield (key, value)
